var toggler = document.getElementsByClassName("caret");
var i;

for (i = 0; i < toggler.length; i++) {
    toggler[i].addEventListener("click", function() {
        this.parentElement.querySelector(".nested").classList.toggle("active");
        this.classList.toggle("caret-down");
    });
}

var swiper1 = new Swiper('.swiper1', {
    slidesPerView: 1,
    spaceBetween: 30,
    loop: true,
    centeredSlides: true,
    autoplay: {
        delay: 1000,
        disableOnInteraction: false,
    },
    pagination: {
        el: '.swiper-pagination1',
        clickable: true,
    },
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
})
var swiper2= new Swiper('.swiper2', {
    slidesPerView: 4,
    // spaceBetween: 30,
    centeredSlides: true,
    pagination: {
        el: '.swiper-pagination2',
        clickable: true,
    },
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
})

var single_swiper1 = new Swiper('.content__slider', {
    spaceBetween: 30,
    loop: true,
    centeredSlides: true,
    direction: 'vertical',
    // autoplay: {
    //     delay: 1000,
    //     disableOnInteraction: false,
    // },
    // pagination: {
    //     el: '.swiper-pagination1',
    //
    // },
    navigation: {
        nextEl: '.single-wrapper-next',
        prevEl: '.single-wrapper-prev',
        clickable: true,
    },
})

var sidebar_swiper1 = new Swiper('.sidebar__slider-container', {
    spaceBetween: 30,
    loop: true,
    centeredSlides: true,
    // direction: 'vertical',
    // autoplay: {
    //     delay: 1000,
    //     disableOnInteraction: false,
    // },
    // pagination: {
    //     el: '.swiper-pagination1',
    //
    // },
    navigation: {
        nextEl: '.sidebar-button-next',
        prevEl: '.single-wrapper-prev',
        clickable: true,
    },
})

function myMap() {
    var mapProp= {
        center:new google.maps.LatLng(51.508742,-0.120850),
        zoom:5,
    };
    var map = new google.maps.Map(document.getElementById("googleMap"),mapProp);
}

